package com.besheater.training.javafundamentals.task5.entity;

import com.besheater.training.javafundamentals.task5.repository.IngredientRepository;
import com.besheater.training.javafundamentals.task5.repository.PizzaTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PizzaTest {
    private Client client;
    private Order order;
    // Pizza types
    private PizzaType normal;
    private PizzaType calzone;
    // Ingredients
    private Ingredient tomatoPaste;
    private Ingredient cheese;
    private Ingredient salami;
    private Ingredient bacon;
    private Ingredient garlic;
    private Ingredient corn;
    private Ingredient pepperoni;
    private Ingredient olives;
    private Set<Ingredient> ingredients3;
    private Set<Ingredient> ingredients7;
    private Set<Ingredient> ingredients8;

    @BeforeEach
    public void makeClientAndOrder() {
        client = new Client(147, "John", "Doe", "+123456");
        order = new Order(client);
    }

    @BeforeEach
    public void makePizzaType() {
        PizzaTypeRepository pizzaTypeRepository = new PizzaTypeRepository();
        normal = pizzaTypeRepository.findByName("Normal").get();
        calzone = pizzaTypeRepository.findByName("Calzone").get();
    }

    @BeforeEach
    public void makeIngredients() {
        IngredientRepository ingredientRepository = new IngredientRepository();
        tomatoPaste = ingredientRepository.findByName("Tomato Paste").get();
        cheese = ingredientRepository.findByName("Cheese").get();
        salami = ingredientRepository.findByName("Salami").get();
        bacon = ingredientRepository.findByName("Bacon").get();
        garlic = ingredientRepository.findByName("Garlic").get();
        corn = ingredientRepository.findByName("Corn").get();
        pepperoni = ingredientRepository.findByName("Pepperoni").get();
        olives = ingredientRepository.findByName("Olives").get();

        ingredients3 = new HashSet<>(Arrays.asList(tomatoPaste, cheese, salami));
        ingredients7 = new HashSet<>(Arrays.asList(tomatoPaste, cheese, salami,
                bacon, garlic, corn, pepperoni));
        ingredients8 = new HashSet<>(Arrays.asList(tomatoPaste, cheese, salami,
                bacon, garlic, corn, pepperoni, olives));
    }

    @Test
    public void constructor_NullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(null, order, "Bella", normal, 1));
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, null, "Bella", normal, 1));
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, null, normal, 1));
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", null, 1));
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(null, null, null, null, 1));

        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", normal, 1, null));
    }

    @Test
    public void constructor_InvalidAmount_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", normal, 0));
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", normal, 11));
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", normal, -1));

        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", normal, 15, ingredients3));
    }

    @Test
    public void constructor_InvalidIngredients_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Pizza(client, order, "Bella", normal, 15, ingredients8));
    }

    @Test
    void setName_NullArguments_throwsException() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 1);
        assertThrows(NullPointerException.class, () -> pizza.setName(null));
    }

    @Test
    void setName_invalidName_correctsAsExpected() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 1);
        String correctedName = client.getId() + "_1";
        pizza.setName("");
        assertEquals(correctedName, pizza.getName());
        pizza.setName("123");
        assertEquals(correctedName, pizza.getName());
        pizza.setName("01234567890123456789*");
        assertEquals(correctedName, pizza.getName());
    }

    @Test
    void setName_validName_setsNewName() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 1);
        pizza.setName("1234");
        assertEquals("1234", pizza.getName());
        pizza.setName("Kella mana *");
        assertEquals("Kella mana *", pizza.getName());
        pizza.setName("01234567890123456789");
        assertEquals("01234567890123456789", pizza.getName());
    }

    @Test
    void setAmount_invalidValue_throwsException() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 1);
        assertThrows(IllegalArgumentException.class, () -> pizza.setAmount(0));
        assertThrows(IllegalArgumentException.class, () -> pizza.setAmount(11));
        assertThrows(IllegalArgumentException.class, () -> pizza.setAmount(-1));
        assertThrows(IllegalArgumentException.class, () -> pizza.setAmount(987));
    }

    @Test
    void setAmount_validAmount_setsNewAmount() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);
        pizza.setAmount(1);
        assertEquals(1, pizza.getAmount());
        pizza.setAmount(7);
        assertEquals(7, pizza.getAmount());
        pizza.setAmount(10);
        assertEquals(10, pizza.getAmount());
    }

    @Test
    void getIngredients_tryToModifyCollection_throwsException() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3, ingredients3);
        Set<Ingredient> ingredients = pizza.getIngredients();
        assertThrows(UnsupportedOperationException.class, () -> ingredients.add(olives));
        assertThrows(UnsupportedOperationException.class, () -> ingredients.remove(tomatoPaste));
        assertThrows(UnsupportedOperationException.class, () -> ingredients.clear());
    }

    @Test
    void addIngredient_TooMuchIngredients_throwsException() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3, ingredients7);
        assertThrows(IllegalArgumentException.class, () -> pizza.addIngredient(olives));
    }

    @Test
    void addIngredient_alredyExistingIngredient_throwsException() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);
        pizza.addIngredient(olives);
        assertThrows(IllegalArgumentException.class, () -> pizza.addIngredient(olives));
    }

    @Test
    void addIngredient_sampleIngredients_addsToIngredients() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);
        pizza.addIngredient(tomatoPaste);
        assertTrue(pizza.getIngredients().contains(tomatoPaste));
        pizza.addIngredient(cheese);
        assertTrue(pizza.getIngredients().contains(cheese));
        pizza.addIngredient(corn);
        assertTrue(pizza.getIngredients().contains(corn));
    }

    @Test
    void deleteIngredient_ingredientThatNotExistInPizza_returnFalse() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);
        pizza.addIngredient(tomatoPaste);
        assertFalse(pizza.deleteIngredient(olives));
    }

    @Test
    void deleteIngredient_sampleIngredients_deletesIngredient() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);

        Set<Ingredient> ingredients = new HashSet<>();
        assertEquals(ingredients, pizza.getIngredients());

        pizza.addIngredient(olives);
        pizza.addIngredient(tomatoPaste);
        pizza.addIngredient(cheese);
        pizza.deleteIngredient(tomatoPaste);
        ingredients = new HashSet<>(Arrays.asList(olives, cheese));
        assertEquals(ingredients, pizza.getIngredients());

        pizza.deleteIngredient(olives);
        pizza.deleteIngredient(cheese);
        ingredients = new HashSet<>();
        assertEquals(ingredients, pizza.getIngredients());
    }

    @Test
    void getOnePizzaPriceInCents_samplePizzas_returnsOnePizzaPrice() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);
        assertEquals(normal.getPriceInCents(), pizza.getOnePizzaPriceInCents());

        Set<Ingredient> ingredients = new HashSet<>(Arrays.asList(tomatoPaste, olives, corn));
        pizza = new Pizza(client, order, "Margarita", normal, 3, ingredients);
        long ingredientsPrice = tomatoPaste.getPriceInCents() +
                                olives.getPriceInCents() + corn.getPriceInCents();
        long pizzaPrice = normal.getPriceInCents() + ingredientsPrice;
        assertEquals(pizzaPrice, pizza.getOnePizzaPriceInCents());

        ingredients = new HashSet<>(Arrays.asList(cheese));
        pizza = new Pizza(client, order, "Bella", calzone, 3, ingredients);
        pizzaPrice = calzone.getPriceInCents() + cheese.getPriceInCents();
        assertEquals(pizzaPrice, pizza.getOnePizzaPriceInCents());
    }

    @Test
    void getFullPriceInCents_SamplePizzas_returnsFullPrice() {
        Pizza pizza = new Pizza(client, order, "Margarita", normal, 3);
        assertEquals(normal.getPriceInCents() * 3, pizza.getFullPriceInCents());

        pizza = new Pizza(client, order, "Astra", calzone, 1);
        assertEquals(calzone.getPriceInCents(), pizza.getFullPriceInCents());

        Set<Ingredient> ingredients = new HashSet<>(Arrays.asList(tomatoPaste, olives, corn));
        pizza = new Pizza(client, order, "Margarita", normal, 3, ingredients);
        long ingredientsPrice = tomatoPaste.getPriceInCents() +
                olives.getPriceInCents() + corn.getPriceInCents();
        long pizzaPrice = normal.getPriceInCents() + ingredientsPrice;
        assertEquals(pizzaPrice * 3, pizza.getFullPriceInCents());

        ingredients = new HashSet<>(Arrays.asList(cheese));
        pizza = new Pizza(client, order, "Bella", calzone, 10, ingredients);
        pizzaPrice = calzone.getPriceInCents() + cheese.getPriceInCents();
        assertEquals(pizzaPrice * 10, pizza.getFullPriceInCents());
    }
}