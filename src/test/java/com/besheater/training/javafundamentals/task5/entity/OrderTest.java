package com.besheater.training.javafundamentals.task5.entity;

import com.besheater.training.javafundamentals.task5.repository.IngredientRepository;
import com.besheater.training.javafundamentals.task5.repository.PizzaTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    private Client client;
    // Pizza types
    private PizzaType normal;
    private PizzaType calzone;
    // Ingredients
    private Ingredient tomatoPaste;
    private Ingredient cheese;
    private Ingredient salami;
    private Ingredient bacon;
    private Ingredient garlic;
    private Ingredient corn;
    private Ingredient pepperoni;
    private Ingredient olives;
    private Set<Ingredient> ingredients3;
    private Set<Ingredient> ingredients7;
    private Set<Ingredient> ingredients8;

    @BeforeEach
    public void makeClient() {
        client = new Client(147, "John", "Doe", "+123456");
    }

    @BeforeEach
    public void makePizzaType() {
        PizzaTypeRepository pizzaTypeRepository = new PizzaTypeRepository();

        normal = pizzaTypeRepository.findByName("Normal").get();
        calzone = pizzaTypeRepository.findByName("Calzone").get();
    }

    @BeforeEach
    public void makeIngredients() {
        IngredientRepository ingredientRepository = new IngredientRepository();

        tomatoPaste = ingredientRepository.findByName("Tomato Paste").get();
        cheese = ingredientRepository.findByName("Cheese").get();
        salami = ingredientRepository.findByName("Salami").get();
        bacon = ingredientRepository.findByName("Bacon").get();
        garlic = ingredientRepository.findByName("Garlic").get();
        corn = ingredientRepository.findByName("Corn").get();
        pepperoni = ingredientRepository.findByName("Pepperoni").get();
        olives = ingredientRepository.findByName("Olives").get();

        ingredients3 = new HashSet<>(Arrays.asList(tomatoPaste, cheese, salami));
        ingredients7 = new HashSet<>(Arrays.asList(tomatoPaste, cheese, salami,
                                     bacon, garlic, corn, pepperoni));

        ingredients8 = new HashSet<>(Arrays.asList(tomatoPaste, cheese, salami,
                                     bacon, garlic, corn, pepperoni, olives));
    }

    @Test
    public void constructor_NullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> new Order(null));
    }

    @Test
    public void getPizzas_TryToModifyCollection_throwsException() {
        Order order = new Order(client);
        Pizza pizza = new Pizza(client, order, "Mucho grande", normal, 3);
        List<Pizza> pizzas = order.getPizzas();

        assertThrows(UnsupportedOperationException.class, () -> pizzas.add(pizza));
        assertThrows(UnsupportedOperationException.class, () -> pizzas.set(0, pizza));
        assertThrows(UnsupportedOperationException.class, () -> pizzas.remove(pizza));
        assertThrows(UnsupportedOperationException.class, () -> pizzas.remove(1));
        assertThrows(UnsupportedOperationException.class, () -> pizzas.clear());

    }

    @Test
    public void getPizzas_AddValidPizzas_returnAddedPizzas() {
        Order order = new Order(client);

        order.addPizza("Margarita", normal, 1);
        Pizza pizza = order.getPizzas().get(0);
        assertEquals("Margarita", pizza.getName());
        assertEquals(normal, pizza.getType());
        assertEquals(1, pizza.getAmount());

        order.addPizza("Bella19", calzone, 10, ingredients3);
        pizza = order.getPizzas().get(1);
        assertEquals("Bella19", pizza.getName());
        assertEquals(calzone, pizza.getType());
        assertEquals(10, pizza.getAmount());
        assertEquals(ingredients3, pizza.getIngredients());
    }

    @Test
    public void addPizza_NullArguments_throwsException() {
        Order order = new Order(client);

        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza(null, normal, 1));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Sera", null, 2));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza(null, null, 3));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Ona", calzone, 3, null));
    }

    @Test
    public void addPizza_tooMuchIngredients_throwsException() {
        Order order = new Order(client);

        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Coma", normal, 1, ingredients8));
    }

    @Test
    public void addPizza_invalidAmount_throwsException() {
        Order order = new Order(client);

        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Coma", normal, 0));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Coma", normal, 11));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Coma", normal, -1));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Coma", normal, 0, ingredients3));
        assertThrows(IllegalArgumentException.class,
                () -> order.addPizza("Coma", normal, 11, ingredients3));
    }

    @Test
    public void addPizza_invalidName_correctsName() {
        Order order = new Order(client);
        String clientId = order.getClient().getId() + "";

        order.addPizza("012345678901234567890", normal, 3);
        Pizza pizza = order.getPizzas().get(0);
        assertEquals(clientId + "_1", pizza.getName());

        order.addPizza("", calzone, 5);
        pizza = order.getPizzas().get(1);
        assertEquals(clientId + "_2", pizza.getName());

        order.addPizza("123", normal, 2);
        pizza = order.getPizzas().get(2);
        assertEquals(clientId + "_3", pizza.getName());
    }

    @Test
    public void addPizza_validPizzas_addsToOrder() {
        Order order = new Order(client);

        order.addPizza("Seniora", normal, 1);
        Pizza pizza = order.getPizzas().get(0);
        assertEquals("Seniora", pizza.getName());
        assertEquals(normal, pizza.getType());
        assertEquals(1, pizza.getAmount());

        order.addPizza("_*-$", normal, 7);
        pizza = order.getPizzas().get(1);
        assertEquals("_*-$", pizza.getName());
        assertEquals(normal, pizza.getType());
        assertEquals(7, pizza.getAmount());

        order.addPizza("Bella19", calzone, 10, ingredients7);
        pizza = order.getPizzas().get(2);
        assertEquals("Bella19", pizza.getName());
        assertEquals(calzone, pizza.getType());
        assertEquals(10, pizza.getAmount());
        assertEquals(ingredients7, pizza.getIngredients());

        order.addPizza("1234", normal, 9, Collections.emptySet());
        pizza = order.getPizzas().get(3);
        assertEquals("1234", pizza.getName());
        assertEquals(normal, pizza.getType());
        assertEquals(9, pizza.getAmount());
        assertEquals(Collections.emptySet(), pizza.getIngredients());
    }

    @Test
    public void deletePizza_tryToDeletePizzasThatNotExistInThisOrder_returnsFalse() {
        Order order = new Order(client);
        Pizza pizza = new Pizza(client, order, "Bella", normal, 2);
        assertFalse(order.deletePizza(pizza));
    }

    @Test
    public void deletePizza_deleteExistingPizza_removesPizzaFromOrder() {
        Order order = new Order(client);
        order.addPizza("Seniora", normal, 1);
        order.addPizza("Bella", calzone, 2);
        order.addPizza("Margarita", normal, 7);
        List<Pizza> pizzasAfterDeletion = new ArrayList<>();
        pizzasAfterDeletion.add(order.getPizzas().get(0));
        pizzasAfterDeletion.add(order.getPizzas().get(2));
        order.deletePizza(order.getPizzas().get(1));
        assertEquals(pizzasAfterDeletion, order.getPizzas());
    }

    @Test
    public void correctPizzaName_NullArguments_ThrowException() {
        Order order = new Order(client);
        assertThrows(NullPointerException.class, () -> order.correctPizzaName(null));
    }

    @Test
    public void correctPizzaName_InvalidNames_CorrectsAsExpected() {
        Order order = new Order(client);
        String clientId = order.getClient().getId() + "";

        assertEquals(clientId + "_1", order.correctPizzaName(""));
        assertEquals(clientId + "_1", order.correctPizzaName("123"));
        assertEquals(clientId + "_1",
                order.correctPizzaName("01234567890123456789*"));
    }

    @Test
    public void correctPizzaName_validNames_returnsNotCorrected() {
        Order order = new Order(client);

        assertEquals("1234",
                order.correctPizzaName("1234"));
        assertEquals("Margarita ola cola",
                order.correctPizzaName("Margarita ola cola"));
        assertEquals("01234567890123456789",
                order.correctPizzaName("01234567890123456789"));
    }

    @Test
    public void getPriceInCents_AddSamplePizzas_CalculatesAccordingly() {
        Order order = new Order(client);

        long orderPriceInCents = 0;
        assertEquals(orderPriceInCents, order.getPriceInCents());

        order.addPizza("Seniora", normal, 1);
        orderPriceInCents = order.getPizzas().get(0).getFullPriceInCents();
        assertEquals(orderPriceInCents, order.getPriceInCents());

        order.addPizza("Bella", calzone, 5);
        orderPriceInCents += order.getPizzas().get(1).getFullPriceInCents();
        assertEquals(orderPriceInCents, order.getPriceInCents());

        order.addPizza("Cabrone", normal, 9, ingredients7);
        orderPriceInCents += order.getPizzas().get(2).getFullPriceInCents();
        assertEquals(orderPriceInCents, order.getPriceInCents());
    }
}