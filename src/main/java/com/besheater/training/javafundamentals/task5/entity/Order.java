package com.besheater.training.javafundamentals.task5.entity;

import java.text.NumberFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Order {
    private static final int MIN_PIZZA_NAME_LENGTH = 4;
    private static final int MAX_PIZZA_NAME_LENGTH = 20;
    private static final AtomicLong nextId = new AtomicLong(10000);
    // I would love to use Locale.ITALY but they changed default l10n pattern
    // see https://bugs.openjdk.java.net/browse/JDK-8203773
    private static final NumberFormat PRICE_FORMAT =
                         NumberFormat.getCurrencyInstance(Locale.GERMANY);
    private final long id;
    private final Client client;
    private List<Pizza> pizzas;
    private final LocalTime creationTime;

    public Order(Client client) {
        if (client == null) {
            throw new IllegalArgumentException("Client can't be null");
        }
        this.id = nextId.incrementAndGet();
        this.client = client;
        this.pizzas = new ArrayList<>();
        this.creationTime = LocalTime.now();
    }

    public long getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public List<Pizza> getPizzas() {
        return Collections.unmodifiableList(pizzas);
    }

    public LocalTime getCreationTime() {
        return creationTime;
    }

    public void addPizza(String pizzaName, PizzaType type, int amount) {
        Pizza pizza = new Pizza(client, this, pizzaName, type, amount);
        pizzas.add(pizza);
    }

    public void addPizza(String pizzaName, PizzaType type, int amount,
                         Set<Ingredient> ingredients) {
        Pizza pizza = new Pizza(client, this, pizzaName, type, amount, ingredients);
        pizzas.add(pizza);
    }

    public boolean deletePizza(Pizza pizza) {
        return pizzas.remove(pizza);
    }

    public String correctPizzaName(String pizzaName) {
        if (isPizzaNameValid(pizzaName)) {
            return pizzaName;
        }
        return "" + client.getId() + "_" + (pizzas.size() + 1);
    }

    private boolean isPizzaNameValid(String pizzaName) {
        return pizzaName.length() >= MIN_PIZZA_NAME_LENGTH &&
                pizzaName.length() <= MAX_PIZZA_NAME_LENGTH;
    }

    public long getPriceInCents() {
        return pizzas.stream().map(Pizza::getFullPriceInCents)
                              .reduce(Long::sum)
                              .orElse(0L);
    }

    private String formatPrice(long priceInCents) {
        return PRICE_FORMAT.format(priceInCents / 100.0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String orderPrefix =    "*********************************\n";
        String orderSuffix =    "*********************************\n";
        String pizzaPrefix =    "=================================\n";
        String pizzaSuffix =    "=================================\n";
        String infoSeparator =  "---------------------------------\n";

        builder.append(orderPrefix);
        builder.append(String.format("Order ID: %d\n", id));
        builder.append(String.format("Client ID: %d\n", client.getId()));
        for (Pizza pizza : pizzas) {
            builder.append(pizzaPrefix);

            builder.append(String.format("Pizza name: %s\n", pizza.getName()));
            builder.append(infoSeparator);

            String typeName = pizza.getType().getName();
            String typePrice = formatPrice(pizza.getType().getPriceInCents());
            builder.append(String.format("PizzaBase(%s) %s\n", typeName, typePrice));
            for (Ingredient ingredient : pizza.getIngredients()) {
                String name = ingredient.getName();
                String price = formatPrice(ingredient.getPriceInCents());
                builder.append(String.format("%s %s\n", name, price));
            }
            builder.append(infoSeparator);
            String pizzaPrice = formatPrice(pizza.getOnePizzaPriceInCents());
            builder.append(String.format("Price: %s\n", pizzaPrice));
            builder.append(String.format("Amount: %s\n", pizza.getAmount()));
        }
        builder.append(pizzaSuffix);

        String total = formatPrice(getPriceInCents());
        builder.append(String.format("TOTAL: %s\n", total));
        builder.append(orderSuffix);

        return builder.toString();
    }
}