package com.besheater.training.javafundamentals.task5.repository;

import com.besheater.training.javafundamentals.task5.entity.Order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderRepository {
    private Map<Long, Order> orders = new HashMap<>();

    public OrderRepository() {}

    public List<Order> getAll() {
        return new ArrayList<>(orders.values());
    }

    public Order get(long id) {
        return orders.get(id);
    }

    public void add(Order order) {
        if (orders.containsKey(order.getId())) {
            throw new IllegalArgumentException("Order is already exist");
        }
        orders.put(order.getId(), order);
    }

    public void delete(Order order) {
        if (!orders.containsKey(order.getId())) {
            throw new IllegalArgumentException("Order is not exist");
        }
        orders.remove(order.getId());
    }
}