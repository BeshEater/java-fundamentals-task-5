package com.besheater.training.javafundamentals.task5.model;

import com.besheater.training.javafundamentals.task5.entity.Order;
import com.besheater.training.javafundamentals.task5.repository.OrderRepository;

public class OrderManager {
    private final OrderRepository orderRepository;

    public OrderManager(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void executeOrder(Order order) {
        orderRepository.add(order);
    }

    public Order getOrder(long orderId) {
        return orderRepository.get(orderId);
    }
}
