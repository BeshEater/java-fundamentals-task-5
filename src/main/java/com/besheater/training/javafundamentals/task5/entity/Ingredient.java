package com.besheater.training.javafundamentals.task5.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Ingredient {
    private static final AtomicLong nextId = new AtomicLong();
    private final long id;
    private final String name;
    private final long priceInCents;

    public Ingredient(String name, long priceInCents) {
        this.id = nextId.incrementAndGet();
        this.name = name;
        this.priceInCents = priceInCents;
    }

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public long getPriceInCents() {
        return priceInCents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", priceInCents=" + priceInCents +
                '}';
    }
}