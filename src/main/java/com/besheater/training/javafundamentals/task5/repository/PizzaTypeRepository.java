package com.besheater.training.javafundamentals.task5.repository;

import com.besheater.training.javafundamentals.task5.entity.PizzaType;

import java.util.*;

public class PizzaTypeRepository {
    private Map<Long, PizzaType> types = new HashMap<>();

    public PizzaTypeRepository() {
        add(new PizzaType("Normal", 100));
        add(new PizzaType("Calzone", 150));
    }

    public List<PizzaType> getAll() {
        return new ArrayList<>(types.values());
    }

    public PizzaType get(long id) {
        return types.get(id);
    }

    public void add(PizzaType type) {
        if (types.containsKey(type.getId())) {
            throw new IllegalArgumentException("Pizza type is already exist");
        }
        types.put(type.getId(), type);
    }

    public void delete(PizzaType type) {
        if (!types.containsKey(type.getId())) {
            throw new IllegalArgumentException("Pizza type is not exist");
        }
        types.remove(type.getId());
    }

    public Optional<PizzaType> findByName(String name) {
        return types.values().stream()
                             .filter(type -> type.getName().equals(name))
                             .findFirst();
    }
}