package com.besheater.training.javafundamentals.task5.repository;

import com.besheater.training.javafundamentals.task5.entity.Ingredient;

import java.util.*;

public class IngredientRepository {
    private Map<Long, Ingredient> ingredients = new HashMap<>();

    public IngredientRepository() {
        add(new Ingredient("Tomato Paste", 100));
        add(new Ingredient("Cheese", 100));
        add(new Ingredient("Salami", 150));
        add(new Ingredient("Bacon", 120));
        add(new Ingredient("Garlic", 30));
        add(new Ingredient("Corn", 70));
        add(new Ingredient("Pepperoni", 60));
        add(new Ingredient("Olives", 50));
    }

    public List<Ingredient> getAll() {
        return new ArrayList<>(ingredients.values());
    }

    public Ingredient get(long id) {
        return ingredients.get(id);
    }

    public void add(Ingredient ingredient) {
        if (ingredients.containsKey(ingredient.getId())) {
            throw new IllegalArgumentException("Ingredient is already exist");
        }
        ingredients.put(ingredient.getId(), ingredient);
    }

    public void delete(Ingredient ingredient) {
        if (!ingredients.containsKey(ingredient.getId())) {
            throw new IllegalArgumentException("Ingredient is not exist");
        }
        ingredients.remove(ingredient.getId());
    }

    public Optional<Ingredient> findByName(String name) {
        return ingredients.values().stream()
                                   .filter(ingredient -> ingredient.getName().equals(name))
                                   .findFirst();
    }
}