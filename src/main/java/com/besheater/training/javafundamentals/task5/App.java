package com.besheater.training.javafundamentals.task5;

import com.besheater.training.javafundamentals.task5.entity.*;
import com.besheater.training.javafundamentals.task5.model.OrderManager;
import com.besheater.training.javafundamentals.task5.repository.IngredientRepository;
import com.besheater.training.javafundamentals.task5.repository.OrderRepository;
import com.besheater.training.javafundamentals.task5.repository.PizzaTypeRepository;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class App {
    // Manual DI
    private static IngredientRepository ingredientRepository = new IngredientRepository();
    private static PizzaTypeRepository pizzaTypeRepository = new PizzaTypeRepository();
    private static OrderManager orderManager = new OrderManager(new OrderRepository());

    public static void main( String[] args ) throws UnsupportedEncodingException {
        // Use UTF-8 encoding for output to correctly display euro symbol (€)
        PrintStream out = new PrintStream(System.out, true, "UTF-8");

        // Load some items from our menu to work with
        PizzaType normal = pizzaTypeRepository.findByName("Normal").get();
        PizzaType calzone = pizzaTypeRepository.findByName("Calzone").get();
        Ingredient tomatoPaste = ingredientRepository.findByName("Tomato Paste").get();
        Ingredient cheese = ingredientRepository.findByName("Cheese").get();
        Ingredient salami = ingredientRepository.findByName("Salami").get();
        Ingredient bacon = ingredientRepository.findByName("Bacon").get();
        Ingredient garlic = ingredientRepository.findByName("Garlic").get();
        Ingredient corn = ingredientRepository.findByName("Corn").get();
        Ingredient pepperoni = ingredientRepository.findByName("Pepperoni").get();
        Ingredient olives = ingredientRepository.findByName("Olives").get();

        // Client 7717 order runs smoothly
        out.println("Client 7717 order:");
        Client client1 = new Client(7717);
        Order order1 = new Order(client1);
        // Add "Margarita" pizza and then add some ingredients
        order1.addPizza("Margarita", calzone,2);
        Pizza margarita = order1.getPizzas().get(0);
        margarita.addIngredient(salami, olives, cheese, garlic);
        // Add "Pepperoni pizza with ingredients already defined
        Set<Ingredient> pepperoniIngredients = new HashSet<>(Arrays.asList(tomatoPaste, pepperoni));
        order1.addPizza("Pepperoni", normal, 3, pepperoniIngredients);
        // Process order
        orderManager.executeOrder(order1);
        out.println(order1);

        // Client 4372 tries to order too much pizzas and gets warning
        out.println("Client 4372 order:");
        Client client2 = new Client(4372);
        Order order2 = new Order(client2);
        // Add "Base ZZ" pizza
        try {
            order2.addPizza("Base ZZ", calzone,12);
            Pizza baseZZ = order2.getPizzas().get(0);
            baseZZ.addIngredient(bacon, corn, olives);
        } catch (IllegalArgumentException ex) {
            out.println(ex.getMessage());
        }
    }
}