package com.besheater.training.javafundamentals.task5.entity;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Pizza {
    private static final AtomicLong nextId = new AtomicLong();
    private static final int MIN_AMOUNT = 1;
    private static final int MAX_AMOUNT = 10;
    private static final int MAX_INGREDIENTS_SIZE = 7;

    private final long id;
    private final Client client;
    private final Order order;
    private String name;
    private PizzaType type;
    private Set<Ingredient> ingredients;
    private int amount;

    public Pizza(Client client, Order order, String name, PizzaType type, int amount) {
        if (client == null || order == null || name == null || type == null) {
            throw new IllegalArgumentException("Arguments must not be null");
        }
        checkAmount(amount);
        this.id = nextId.incrementAndGet();
        this.client = client;
        this.order = order;
        this.name = order.correctPizzaName(name);
        this.type = type;
        this.ingredients = new HashSet<>();
        this.amount = amount;
    }

    public Pizza(Client client, Order order, String name, PizzaType type, int amount,
                 Set<Ingredient> ingredients) {
        this(client, order, name, type, amount);
        if (ingredients == null) {
            throw new IllegalArgumentException("Ingredients must not be null");
        }
        if (ingredients.size() > MAX_INGREDIENTS_SIZE) {
            String message = String.format("Too many ingredients. " +
                    "You can't have more than %d ingredients", MAX_INGREDIENTS_SIZE);
            throw new IllegalArgumentException(message);
        }
        this.ingredients.addAll(ingredients);
    }

    public long getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public Order getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = order.correctPizzaName(name);
    }

    public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        checkAmount(amount);
        this.amount = amount;
    }

    private void checkAmount(int amount) {
        if (!isAmountValid(amount)) {
            String message = String.format("Amount is not valid. " +
                    "It should be between %d and %d pizzas inclusive.", MIN_AMOUNT, MAX_AMOUNT);
            throw new IllegalArgumentException(message);
        }
    }

    private boolean isAmountValid(int amount) {
        return amount >= MIN_AMOUNT && amount <= MAX_AMOUNT;
    }

    public Set<Ingredient> getIngredients() {
        return Collections.unmodifiableSet(ingredients);
    }

    public void addIngredient(Ingredient ingredient) {
        if (ingredients.contains(ingredient)) {
            String message = String.format("Ingredient %s is already added. " +
                    "You can't add the same ingredient twice", ingredient.getName());
            throw new IllegalArgumentException(message);
        }
        if (ingredients.size() == MAX_INGREDIENTS_SIZE) {
            String message = String.format("Pizza is full, you can't add more than %d ingredients",
                                           MAX_INGREDIENTS_SIZE);
            throw new IllegalArgumentException(message);
        }
        ingredients.add(ingredient);
    }

    public void addIngredient(Ingredient... ingredients) {
        for (Ingredient ingredient : ingredients) {
            addIngredient(ingredient);
        }
    }

    public boolean deleteIngredient(Ingredient ingredient) {
        return ingredients.remove(ingredient);
    }

    public long getOnePizzaPriceInCents() {
        long ingredientsPrice = ingredients.stream().map(Ingredient::getPriceInCents)
                                                    .reduce(Long::sum)
                                                    .orElse(0L);
        return type.getPriceInCents() + ingredientsPrice;
    }

    public long getFullPriceInCents() {
        return getOnePizzaPriceInCents() * amount;
    }

    public String printAttributes() {
        long orderId = order.getId();
        long clientId = client.getId();
        return String.format("[%d : %d : %s: %d]", orderId, clientId, name, amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return id == pizza.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", client=" + client +
                ", order=" + order +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", ingredients=" + ingredients +
                ", amount=" + amount +
                '}';
    }
}